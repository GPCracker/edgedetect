class ConfigReader(object):
	BASE_TYPES = ('Binary', 'Blob', 'Bool', 'Float', 'Int', 'Int64', 'Matrix', 'String', 'Vector2', 'Vector3', 'Vector4', 'WideString')
	
	def readBase(self, typeName, xml, default):
		return getattr(xml, 'as' + typeName) if xml is not None else default
	
	def readDict(self, xml, default):
		getSectionXML = lambda sectionName: xml[sectionName] if xml is not None else None
		return dict([(sectionName, self.readSection(getSectionXML(sectionName), default[sectionName])) for sectionName in default.keys()])
	
	def readExt(self, typeName, xml, default):
		assert typeName in self.__extTypes, 'No reader for {} type'.format(typeName)
		return self.__extTypes[typeName](xml, default)
	
	def readList(self, xml, default, itemName = 'item', itemType = 'String', itemDefault = ''):
		if xml is not None:
			return [self.readSection(xmlSection, (itemType, itemDefault)) for sectionName, xmlSection in xml.items() if sectionName == itemName]
		return [self.readSection(None, (itemType, defaultItem)) for defaultItem in default]
	
	def readCustomDict(self, xml, default, itemType = 'String', itemDefault = ''):
		if xml is not None:
			return dict([(sectionName, self.readSection(xmlSection, (itemType, itemDefault))) for sectionName, xmlSection in xml.items()])
		return dict([(sectionName, self.readSection(None, (itemType, defaultItem))) for sectionName, defaultItem in default.items()])
	
	def readSection(self, xmlSection, defSection):
		if isinstance(defSection, (list, tuple)):
			sectionType, sectionDefault = defSection
			if sectionType in self.BASE_TYPES:
				return self.readBase(sectionType, xmlSection, sectionDefault)
			else:
				return self.readExt(sectionType, xmlSection, sectionDefault)
		elif isinstance(defSection, dict):
			return self.readDict(xmlSection, defSection)
		else:
			raise IOError('Invalid config parameter type "{}".'.format(type(defSection).__name__))
		return None
	
	def readConfig(self, xmlPath, defConfig):
		return self.readSection(ResMgr.openSection(xmlPath), defConfig)
	
	def __init__(self, extTypes = {}):
		self.__extTypes = extTypes
		return None
	
	@property
	def extTypes(self):
		return self.__extTypes
	
	@extTypes.setter
	def extTypes(self, value):
		self.__extTypes = value
		return None
	
	def __del__(self):
		return None
