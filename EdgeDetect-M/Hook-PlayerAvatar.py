#*****
# PlayerAvatar Hooks
#*****
def new_PlayerAvatar_onEnterWorld(self, *args, **kwargs):
	result = old_PlayerAvatar_onEnterWorld(self, *args, **kwargs)
	from helpers import EdgeDetectColorController
	EdgeDetectColorController.g_instance.updateColors()
	return result

def new_PlayerAvatar_targetFocus(self, entity):
	result = old_PlayerAvatar_targetFocus(self, entity)
	import constants
	import Vehicle
	isGuiVisible = self._PlayerAvatar__isGuiVisible
	isInTutorial = self.arena is not None and self.arena.guiType == constants.ARENA_GUI_TYPE.TUTORIAL
	if (isGuiVisible or isInTutorial) and isinstance(entity, Vehicle.Vehicle) and entity.isStarted and entity.isAlive():
		BigWorld.wgDelEdgeDetectEntity(entity)
		if entity is self.autoAimVehicle:
			#autoAim enemy
			colorIndex = _config_['edgeHighlight']['autoAimEnemyColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['autoAimEnemyAllocateInvisiblePartsOnly']
		elif self.team == entity.publicInfo['team']:
			#focused friend
			colorIndex = _config_['edgeHighlight']['targetFriendColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['targetFriendAllocateInvisiblePartsOnly']
		else:
			#focused enemy
			colorIndex = _config_['edgeHighlight']['targetEnemyColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['targetEnemyAllocateInvisiblePartsOnly']
		if colorIndex >= 0:
			BigWorld.wgAddEdgeDetectEntity(entity, colorIndex, allocateInvisiblePartsOnly)
	return result

def new_PlayerAvatar_targetBlur(self, prevEntity):
	result = old_PlayerAvatar_targetBlur(self, prevEntity)
	import constants
	import Vehicle
	isGuiVisible = self._PlayerAvatar__isGuiVisible
	isInTutorial = self.arena is not None and self.arena.guiType == constants.ARENA_GUI_TYPE.TUTORIAL
	if (isGuiVisible or isInTutorial) and isinstance(prevEntity, Vehicle.Vehicle) and prevEntity.isStarted and prevEntity.isAlive():
		BigWorld.wgDelEdgeDetectEntity(prevEntity)
		if prevEntity is self.autoAimVehicle:
			#autoAim enemy
			colorIndex = _config_['edgeHighlight']['autoAimEnemyColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['autoAimEnemyAllocateInvisiblePartsOnly']
		elif self.team == prevEntity.publicInfo['team']:
			#blur friend
			colorIndex = _config_['edgeHighlight']['friendColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['friendAllocateInvisiblePartsOnly']
		else:
			#blur enemy
			colorIndex = _config_['edgeHighlight']['enemyColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['enemyAllocateInvisiblePartsOnly']
		if colorIndex >= 0:
			BigWorld.wgAddEdgeDetectEntity(prevEntity, colorIndex, allocateInvisiblePartsOnly)
	return result

def new_PlayerAvatar_setVisibleGUI(self, bool):
	result = old_PlayerAvatar_setVisibleGUI(self, bool)
	import Vehicle
	playerVehicle = BigWorld.entity(self.playerVehicleID)
	if isinstance(playerVehicle, Vehicle.Vehicle) and playerVehicle.isStarted and playerVehicle.isAlive() and bool:
		BigWorld.wgDelEdgeDetectEntity(playerVehicle)
		colorIndex = _config_['edgeHighlight']['selfColorIndex']
		allocateInvisiblePartsOnly = _config_['edgeHighlight']['selfAllocateInvisiblePartsOnly']
		BigWorld.wgAddEdgeDetectEntity(playerVehicle, colorIndex, allocateInvisiblePartsOnly)
	return result

def new_PlayerAvatar_autoAimVehID_getter(self):
	return self.__autoAimVehID if hasattr(self, '__autoAimVehID') else 0

def new_PlayerAvatar_autoAimVehID_setter(self, value):
	import Vehicle
	oldAutoAimVehicle = BigWorld.entity(self.__autoAimVehID) if hasattr(self, '__autoAimVehID') else None
	self.__autoAimVehID = value
	newAutoAimVehicle = BigWorld.entity(self.__autoAimVehID)
	if isinstance(oldAutoAimVehicle, Vehicle.Vehicle) and oldAutoAimVehicle.isStarted and oldAutoAimVehicle.isAlive():
		BigWorld.wgDelEdgeDetectEntity(oldAutoAimVehicle)
		if oldAutoAimVehicle is BigWorld.target():
			if oldAutoAimVehicle.publicInfo['team'] == BigWorld.player().team:
				#focused friend
				colorIndex = _config_['edgeHighlight']['targetFriendColorIndex']
				allocateInvisiblePartsOnly = _config_['edgeHighlight']['targetFriendAllocateInvisiblePartsOnly']
			else:
				#focused enemy
				colorIndex = _config_['edgeHighlight']['targetEnemyColorIndex']
				allocateInvisiblePartsOnly = _config_['edgeHighlight']['targetEnemyAllocateInvisiblePartsOnly']
		else:
			if oldAutoAimVehicle.publicInfo['team'] == BigWorld.player().team:
				#blur friend
				colorIndex = _config_['edgeHighlight']['friendColorIndex']
				allocateInvisiblePartsOnly = _config_['edgeHighlight']['friendAllocateInvisiblePartsOnly']
			else:
				#blur enemy
				colorIndex = _config_['edgeHighlight']['enemyColorIndex']
				allocateInvisiblePartsOnly = _config_['edgeHighlight']['enemyAllocateInvisiblePartsOnly']
		if colorIndex >= 0:
			BigWorld.wgAddEdgeDetectEntity(oldAutoAimVehicle, colorIndex, allocateInvisiblePartsOnly)
	if isinstance(newAutoAimVehicle, Vehicle.Vehicle) and newAutoAimVehicle.isStarted and newAutoAimVehicle.isAlive():
		BigWorld.wgDelEdgeDetectEntity(newAutoAimVehicle)
		colorIndex = _config_['edgeHighlight']['autoAimEnemyColorIndex']
		allocateInvisiblePartsOnly = _config_['edgeHighlight']['autoAimEnemyAllocateInvisiblePartsOnly']
		if colorIndex >= 0:
			BigWorld.wgAddEdgeDetectEntity(newAutoAimVehicle, colorIndex, allocateInvisiblePartsOnly)
	return None
