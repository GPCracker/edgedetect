#*****
# EdgeDetectColorController Hooks
#*****
def new_EdgeDetectColorController_changeColor(self, diff):
	from Account import PlayerAccount
	if isinstance(BigWorld.player(), PlayerAccount):
		return old_EdgeDetectColorController_changeColor(self, diff)
	color0 = _config_['edgeHighlight']['color0'].scale(1.0 / 255.0)
	color1 = _config_['edgeHighlight']['color1'].scale(1.0 / 255.0)
	color2 = _config_['edgeHighlight']['color2'].scale(1.0 / 255.0)
	color3 = _config_['edgeHighlight']['color3'].scale(1.0 / 255.0)
	BigWorld.wgSetEdgeDetectColors((color0, color1, color2, color3))
	return
