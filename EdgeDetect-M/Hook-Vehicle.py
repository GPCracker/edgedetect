#*****
# Vehicle Hooks
#*****
def new_Vehicle_startVisual(self):
	result = old_Vehicle_startVisual(self)
	if self.isStarted and self.isAlive():
		BigWorld.wgDelEdgeDetectEntity(self)
		if self.isPlayer:
			#self
			colorIndex = _config_['edgeHighlight']['selfColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['selfAllocateInvisiblePartsOnly']
		elif BigWorld.player().team == self.publicInfo['team']:
			#blur ally
			colorIndex = _config_['edgeHighlight']['friendColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['friendAllocateInvisiblePartsOnly']
		else:
			#blur enemy
			colorIndex = _config_['edgeHighlight']['enemyColorIndex']
			allocateInvisiblePartsOnly = _config_['edgeHighlight']['enemyAllocateInvisiblePartsOnly']
		if colorIndex >= 0:
			BigWorld.wgAddEdgeDetectEntity(self, colorIndex, allocateInvisiblePartsOnly)
	return result

def new_Vehicle_stopVisual(self):
	if self.isStarted and self.isAlive():
		BigWorld.wgDelEdgeDetectEntity(self)
	return old_Vehicle_stopVisual(self)

def new_Vehicle_onVehicleDeath(self, *args, **kwargs):
	result = old_Vehicle_onVehicleDeath(self, *args, **kwargs)
	if self.isStarted:
		BigWorld.wgDelEdgeDetectEntity(self)
	return result
