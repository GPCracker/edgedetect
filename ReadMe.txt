#Disclaimer:
#	This file was written in Russian, and partially translated by special software.
#	Configuration file was translated by author himself in case of preventing data distortion.
#	Author is not responsible for the accuracy of any other translations except Russian below.

Подсветка контуров техники / Edge Detect Mod
Модификация для World of Tanks / Modification for World of Tanks

Автор / Author: GPCracker

[Описание модификации]
Модификация может изменять цвета и условия подсветки контуров техники, появляющихся, например, при наведении на нее. Подсветка контуров может работать в двух режимах: обводка по контуру и обводка невидимых участков.
Например, с помощью модификации можно сделать цвет обводки союзников скажем бирюзовым, и сделать подсветку контуров у союзников постоянной. Что существенно снижает количество попаданий по союзникам случайно вылетевшим под ваш снаряд из-за угла. Или добавить подсветку цели в автоприцеле, что позволит вам точно знать кого вы в суматохе боя захватили в автоприцел. Можно также изменить цвет подсветки противников, и даже своего танка, когда вы въезжаете в кусты или стоите за небольшой преградой.

[Важная информация]
Конфигурация по умолчанию полностью повторяет игровую. Не удивляйтесь, если вы не видите изменений. Изменяйте параметры в файле конфигурации.
Некоторые функции модификации могут нарушать правила игры. Советуем Вам прочитать их перед редактированием файла конфигурации.
Загрузчики скриптов с модификацией не поставляются. Вы ищете и устанавливаете их самостоятельно.

[Редактирование файла конфигурации]
Файл конфигурации нельзя редактировать стандартным блокнотом, входящим в Windows, а также другими простыми текстовыми редакторами, не поддерживающими кодировку файла конфигурации - UTF-8 w/o BOM.
Все параметры файла конфигурации имеют комментарии на русском и английском языках. Если у параметра нет комментария, значит этот параметр определен выше. Ищите одноименный параметр / секцию.

[Параметры файла конфигурации]
Цвет: 4 числа [0..255] через пробел (RGB + Alpha(непрозрачность))
Индекс: [-1..2] (индекс -1 - контур не подсвечивается)
Более 3 цветов не поддерживается движком игры.

[Description of modification]
Modification can change colors and conditions of illumination of the contours of vehicles appearing, for example, when aiming at it. Illumination of contours can work in two modes: an inking on a contour and an inking of invisible sites.
For example, by means of modification it is possible to make color of an inking of allies we will tell turquoise, and to make illumination of contours at allies of a constant. That significantly reduces number of hits on the allies who incidentally took off under your shell from round the corner. Or to add illumination of the purpose in an autosight that will allow you to know precisely whom you in turmoil of fight took in an autosight. It is also possible to change color of illumination of opponents, and even the tank when you drive in bushes or standing behind a small barrier.

[Important information]
The configuration by default completely repeats the game. Be not surprised if you don't see changes. Change parameters in the configuration file.
Some functions of modification can break rules of the game. We advise you to read them before editing the file of a configuration.
Loaders of scripts with modification aren't delivered. You look for and you establish them independently.

[Editing file of a configuration]
It is impossible to edit the configuration file with the standard notepad included in Windows, and also with other simple text editors which aren't supporting the encoding of the configuration file - UTF-8 w/o BOM.
All parameters of the configuration file have comments in the Russian and English languages. If parameter has no comment, this parameter means is determined above. Look for the parameter / section of the same name.

[Parameters of the configuration file]
Color: 4 numbers [0..255] through a gap (RGB + Alpha (opacity))
Index: [-1..2] (an index -1 - the contour isn't highlighted)
More than 3 colors aren't maintained by a game engine.
