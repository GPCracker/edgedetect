#*****
# Configuration
#*****
_config_ = None

#*****
# Default configuration
#*****
def defaultConfig():
	return {
		'modEnabled': ('Bool', True),
		'ignoreClientVersion': ('Bool', False),
		'hookSetTimeout': ('Float', 3.0),
		'modLoadedMessage': ('WideString', u'{} loaded.'.format(__application__)),
		'modUpdateMessage': ('WideString', u'Please update {}!'.format(__application__)),
		'edgeHighlight': {
			'color0': ('Vector4', Math.Vector4(127, 127, 127, 255)),
			'color1': ('Vector4', Math.Vector4(255, 18, 7, 255)),
			'color2': ('Vector4', Math.Vector4(124, 214, 6, 255)),
			'color3': ('Vector4', Math.Vector4(255, 255, 255, 255)),
			'selfColorIndex': ('Int', 0),
			'selfAllocateInvisiblePartsOnly': ('Bool', True),
			'enemyColorIndex': ('Int', -1),
			'enemyAllocateInvisiblePartsOnly': ('Bool', False),
			'targetEnemyColorIndex': ('Int', 1),
			'targetEnemyAllocateInvisiblePartsOnly': ('Bool', False),
			'autoAimEnemyColorIndex': ('Int', -1),
			'autoAimEnemyAllocateInvisiblePartsOnly': ('Bool', False),
			'friendColorIndex': ('Int', -1),
			'friendAllocateInvisiblePartsOnly': ('Bool', False),
			'targetFriendColorIndex': ('Int', 2),
			'targetFriendAllocateInvisiblePartsOnly': ('Bool', False)
		}
	}

#*****
# Read configuration from file
#*****
def readConfig():
	mainSection = ResMgr.openSection(__file__.replace('.pyc', '.xml'))
	if mainSection is None:
		print '[{}] Config loading failed.'.format(__appShortName__)
	else:
		print '[{}] Config successfully loaded.'.format(__appShortName__)
	return ConfigReader().readSection(mainSection, defaultConfig())
