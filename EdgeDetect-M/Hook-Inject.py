#*****
# Injecting Hooks
#*****
def injectHooks():
	from Avatar import PlayerAvatar
	global old_PlayerAvatar_onEnterWorld
	old_PlayerAvatar_onEnterWorld = PlayerAvatar.onEnterWorld
	PlayerAvatar.onEnterWorld = new_PlayerAvatar_onEnterWorld
	global old_PlayerAvatar_targetFocus
	old_PlayerAvatar_targetFocus = PlayerAvatar.targetFocus
	PlayerAvatar.targetFocus = new_PlayerAvatar_targetFocus
	global old_PlayerAvatar_targetBlur
	old_PlayerAvatar_targetBlur = PlayerAvatar.targetBlur
	PlayerAvatar.targetBlur = new_PlayerAvatar_targetBlur
	global old_PlayerAvatar_setVisibleGUI
	old_PlayerAvatar_setVisibleGUI = PlayerAvatar._PlayerAvatar__setVisibleGUI
	PlayerAvatar._PlayerAvatar__setVisibleGUI = new_PlayerAvatar_setVisibleGUI
	PlayerAvatar._PlayerAvatar__autoAimVehID = property(new_PlayerAvatar_autoAimVehID_getter, new_PlayerAvatar_autoAimVehID_setter)
	from Vehicle import Vehicle
	global old_Vehicle_startVisual
	old_Vehicle_startVisual = Vehicle.startVisual
	Vehicle.startVisual = new_Vehicle_startVisual
	global old_Vehicle_stopVisual
	old_Vehicle_stopVisual = Vehicle.stopVisual
	Vehicle.stopVisual = new_Vehicle_stopVisual
	global old_Vehicle_onVehicleDeath
	old_Vehicle_onVehicleDeath = Vehicle._Vehicle__onVehicleDeath
	Vehicle._Vehicle__onVehicleDeath = new_Vehicle_onVehicleDeath
	from helpers.EdgeDetectColorController import EdgeDetectColorController
	global old_EdgeDetectColorController_changeColor
	old_EdgeDetectColorController_changeColor = EdgeDetectColorController._EdgeDetectColorController__changeColor
	EdgeDetectColorController._EdgeDetectColorController__changeColor = new_EdgeDetectColorController_changeColor
	return None
